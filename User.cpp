#include "User.h"

User::User(std::string userName,SOCKET sock)
{
	this->_userName = userName;
	this->_sock = sock;
}

std::string User::getUserName()
{
	return this->_userName;
}

SOCKET User::getSocket()
{
	return this->_sock;
}

