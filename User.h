
#pragma once
#include "Helper.h"
class User {
public:
	User(std::string,SOCKET);
	std::string getUserName();
	//void send(std::string message); //sends message to user using Helper::send data
	SOCKET getSocket();
	
private:
	std::string _userName;
	SOCKET _sock;
};
