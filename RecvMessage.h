#pragma once

#include "User.h"

using namespace std;

class RecvMessage
{
public:
	RecvMessage(SOCKET sock, int messageCode);

	RecvMessage(SOCKET sock, int messageCode, vector<string> values);
	
	SOCKET getSock();
	User* getUser();
	void setUser(User*);
	int getMessageCode();

	vector<string>& getValues();

private:
	User * _user;
	SOCKET _sock;
	int _messageCode;
	vector<string> _values;
};
