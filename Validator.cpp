#include "Validator.h"

bool Validator::isPassValid(std::string pass)
{
	
	if ((std::count_if(pass.begin(), pass.end(), [](char ch) { return islower(ch); }) == 1) && (std::count_if(pass.begin(), pass.end(), [](char ch) { return isupper(ch); }) == 1) && (std::count(pass.begin(), pass.end(), ' ') == 0)&&(pass.length()>=4))
	{
		return true;
	}
	else {
		return false;
	}
}

bool Validator::isUserNameValid(std::string name)
{
	bool letter = false;
	bool isOK=false;
	int i = 0;
	if (!isalpha(name[0]))
	{
		letter = true;
	}
	int spaces = std::count(name.begin(), name.end(), ' ');
	if ((letter == false) && (spaces == 0 )&& (name.length() > 0))
	{
		return true;
		isOK = true;
	}
	if (isOK != true)
	{
		return false;
	}
}