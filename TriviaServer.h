#pragma once

#include "RecvMessage.h"
#include "Helper.h"
#include "User.h"
#include "Room.h"
#include "Validator.h"
class TriviaServer
{
public:
	TriviaServer(); //creates new socket
	~TriviaServer();//d'tor. clears connected users.close the socket
	void serve();//starts the function "bindAndListen". starts the thread clientHandler. while (1) for receivng clients using accept function
	void bindAndListen();
	void accept();//creates a thread for a connected client. passes the socket of the client
	void clientHandler(SOCKET client_socket);/*receives the message code from helper function.
											// while the message is not end message or 0:
											 //build message using buildReceiveMessage and add to queue with add function to queue
											 //create end message and add to queue
											 //*/
	RecvMessage* buildReceiveMessage(SOCKET client_socket,int msgCode);//build a message. 
															   //if there is something else then code,add to vector
	User* getUserByName(std::string userName);
	User* getUserBySocket(SOCKET client_socket);
	void handleRecievedMessage();//handles the messages that are currently in the queue
	void print();
	void addRecievedMessages(RecvMessage*);
	int isSignUpLegal(string userName,string pass);
private:
	int msgCode;
	SOCKET _socket;
	std::mutex _mtxRecievedMessages;
	std::queue<RecvMessage*> _messageHandler;
	std::condition_variable _msgQueueCondition;
	std::deque<pair<SOCKET, string>> _clients;
	int roomNum = 0;
	std::vector <Room*> roomPointer;
	std::map<SOCKET, User*> UserMap;
	std::map <std::string, std::string> userMap;
	//int playersNumber = 0;
};