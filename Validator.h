#pragma once
#include <stdio.h>
#include <string>
#include <algorithm>
#include <ctype.h>

class Validator {
public:
	bool isPassValid(std::string);
	bool isUserNameValid(std::string);
private:
	std::string _password;
	std::string _name;
};