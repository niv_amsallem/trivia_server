#define _CRT_SECURE_NO_WARNINGS

#include "TriviaServer.h"
#include <exception>
#include <iostream>
#include <string>
#include "WSAInitializer.h"
#pragma comment (lib, "ws2_32.lib")






TriviaServer::TriviaServer()
{
	// notice that we step out to the global namespace
	// for the resolution of the function socket
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}
TriviaServer::~TriviaServer()
{

	// why is this try necessarily ?
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);
	}
	catch (...) {}
}

void TriviaServer::serve()
{
	bindAndListen();
	// create new thread for handling message
	std::thread tr(&TriviaServer::handleRecievedMessage, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TriviaServer::accept();
	}
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(8820);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = 0;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening" << std::endl;
}

void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	std::cout << "Client accepted !" << std::endl;
	// create new thread for client	and detach from it
	std::thread tr(&TriviaServer::clientHandler, this, client_socket);
	tr.detach();

}
void TriviaServer::clientHandler(SOCKET clientSocket)
{
	RecvMessage* currRcvMsg = nullptr;
	try
	{
		// get the first message code
		int msgCode = Helper::getMessageTypeCode(clientSocket);

		while (msgCode != 0)
		{
			currRcvMsg = buildReceiveMessage(clientSocket, msgCode);
			addRecievedMessages(currRcvMsg);

			msgCode = Helper::getMessageTypeCode(clientSocket);
		}

		currRcvMsg = buildReceiveMessage(clientSocket, 299);
		addRecievedMessages(currRcvMsg);

	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function clientHandler. socket=" << clientSocket << ", what=" << e.what() << std::endl;
		currRcvMsg = buildReceiveMessage(clientSocket, 299);
		addRecievedMessages(currRcvMsg);
	}
	closesocket(clientSocket);

}


void TriviaServer::addRecievedMessages(RecvMessage * message)
{

	unique_lock<mutex> lck(_mtxRecievedMessages);

	_messageHandler.push(message);
	lck.unlock();
	_msgQueueCondition.notify_all();
}

RecvMessage* TriviaServer::buildReceiveMessage(SOCKET client_socket, int msgCode)
{
	RecvMessage* msg = nullptr;
	vector<string> values;
	///*******************************************************************************************************
	if (msgCode == 203)//if it is sign up message
	{
		int userNameSize = Helper::getIntPartFromSocket(client_socket, 2);//reads 2 bytes after message code
		string UserName = Helper::getStringPartFromSocket(client_socket, userNameSize);//starts reading n bytes after the previous stopped
		int pasSize = Helper::getIntPartFromSocket(client_socket, 2);
		string Pass = Helper::getStringPartFromSocket(client_socket, pasSize);
		int mailSize = Helper::getIntPartFromSocket(client_socket, 2);
		string mail = Helper::getStringPartFromSocket(client_socket, mailSize);
		values.push_back(UserName);
		values.push_back(Pass);
		values.push_back(mail);
	}
	///*******************************************************************************************************


	///*******************************************************************************************************
	else if (msgCode == 200)//if it is sign in message
	{
		int userNameSize = Helper::getIntPartFromSocket(client_socket, 2);//reads 2 bytes after message code
		string UserName = Helper::getStringPartFromSocket(client_socket, userNameSize);//starts reading n bytes after the previous stopped
		int pasSize = Helper::getIntPartFromSocket(client_socket, 2);
		string Pass = Helper::getStringPartFromSocket(client_socket, pasSize);
		values.push_back(UserName);
		values.push_back(Pass);
	}
	///*******************************************************************************************************



	///*******************************************************************************************************
	else if (msgCode == 213)//if it is create room message
	{
		int roomID = 1;
		int roomNameSize = Helper::getIntPartFromSocket(client_socket, 2);//reads 2 bytes after message code
		string RoomName = Helper::getStringPartFromSocket(client_socket, roomNameSize);//starts reading n bytes after the previous stopped
		int playersNumber = Helper::getIntPartFromSocket(client_socket, 1);
		int questionNumber = Helper::getIntPartFromSocket(client_socket, 2);
		int questionTime = Helper::getIntPartFromSocket(client_socket, 2);
		values.push_back(std::to_string(roomID));
		values.push_back(RoomName);
		values.push_back(std::to_string(playersNumber));
		values.push_back(std::to_string(questionNumber));
		values.push_back(std::to_string(questionTime));
	}
	///*******************************************************************************************************

	///*******************************************************************************************************
	
	else if (msgCode == 205)// list of rooms
	{

		int code = 106;
		std::string message = std::to_string(code) + Helper::getPaddedNumber(this->roomNum, 4);
		//need: 106,roomNum,roomID,RoomName,
		for (auto const& value : this->roomPointer) {
			std::string RoomID = Helper::getPaddedNumber(value->getId(), 4);
			std::string roomNameSize = Helper::getPaddedNumber(value->getName().length(), 2);
			std::string roomName = value->getName();
			message += RoomID + roomNameSize + roomName;
		}
		values.push_back(message);
	}
	///*******************************************************************************************************
	else if (msgCode == 207)
	{
		int roomID = Helper::getIntPartFromSocket(client_socket, 4);
		values.push_back(to_string(roomID));
	}

	else if (msgCode == 209)
	{
		int roomID = Helper::getIntPartFromSocket(client_socket, 4);
		values.push_back(to_string(roomID));
	}
	msg = new RecvMessage(client_socket, msgCode, values);
	return msg;
}



void TriviaServer::print()
{
	for (auto it = this->_clients.begin(); it != this->_clients.end(); ++it)
	{
		cout << ' ' << it->first << ' ' << it->second << endl;

	}
}

void main()
{
	try
	{
		WSAInitializer wsa_init;
		TriviaServer t;
		t.serve();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was thrown in function: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in main !" << std::endl;
	}
}


void TriviaServer::handleRecievedMessage()
{
	int msgCode = 0;
	SOCKET clientSock;
	string userName = "";
	string password = "";
	while (true)
	{
		try
		{
			unique_lock<mutex> lck(_mtxRecievedMessages);
			// Wait for clients to enter the queue.
			if (_messageHandler.empty()) {
				_msgQueueCondition.wait(lck);
			}

			// in case the queue is empty.
			if (_messageHandler.empty())
				continue;

			RecvMessage* currMessage = _messageHandler.front();
			_messageHandler.pop();
			lck.unlock();
			// Extract the data from the tuple.
			clientSock = currMessage->getSock();
			msgCode = currMessage->getMessageCode();

			///*********************************************************************************
			if (msgCode == 203)// if it is sign up message
			{
				userName = currMessage->getValues()[0];
				password = currMessage->getValues()[1];
				string response = to_string(isSignUpLegal(userName, password));
				if (response != "1040") {
					Helper::sendData(clientSock, response);
				}
				else {
					userMap[userName] = password;
					User* u = new User(userName, clientSock);
					UserMap[clientSock] = u;
					_clients.push_back(make_pair(clientSock, userName));
					Helper::sendData(clientSock, "1040");
				}
			}
			///*********************************************************************************

			///*********************************************************************************
			else if (msgCode == 200)
			{
				int counter = 0;
				userName = currMessage->getValues()[0];
				password = currMessage->getValues()[1];
				for (auto const& x : this->userMap)
				{
					if (x.first==userName&&x.second==password)
					{
						//if all the details match up...
						counter=1;
					}
					else {
						//Helper::sendData(clientSock, "1021");
						counter--;
					}
				}
				if (counter == 1)
				{
					Helper::sendData(clientSock, "1020");
					counter = 0;
				}
				else {
					Helper::sendData(clientSock, "1021");
				}
			}
			///*********************************************************************************
			else if (msgCode == 213)
			{
				string id = currMessage->getValues()[0];
				int roomID = atoi(id.c_str());
				string RoomName = currMessage->getValues()[1];
				string number = currMessage->getValues()[2];
				int playersNumber = atoi(number.c_str());
				string qNumber = currMessage->getValues()[3];
				int questionNumber = atoi(qNumber.c_str());
				string qTime = currMessage->getValues()[4];
				int questionTime = atoi(qTime.c_str());
				Room *r = new Room(roomID, getUserBySocket(clientSock), RoomName, playersNumber, questionNumber, questionTime);
				Helper::sendData(clientSock, "1140");
				roomID++;
				this->roomNum++;
				this->roomPointer.push_back(r);
			}
			///*********************************************************************************
			
			///*********************************************************************************
			if (msgCode == 205)
			{
				string message = currMessage->getValues()[0];
				Helper::sendData(clientSock, message);
			}
			
			///*********************************************************************************

			else if (msgCode == 207)//if it asks to display room member list (always happens)
			{
				string room = currMessage->getValues()[0];
				int roomID = atoi(room.c_str());
				for (auto const& value : this->roomPointer)
				{
					if (value->getId() == roomID)
					{
						Helper::sendData(clientSock, value->getUsersListMessage());
					}
				}
			}
			else if (msgCode == 209)//join message
			{
				string room = currMessage->getValues()[0];
				int roomID = atoi(room.c_str());
				for (auto const& value : this->roomPointer)
				{
					if (value->getId() == roomID)
					{
						Helper::sendData(clientSock, value->joinRoomMessage(getUserBySocket(clientSock)));
					}
				}
			}

			delete currMessage;

		}
		catch (...)
		{

		}
	}
}




int TriviaServer::isSignUpLegal(string userName,string pass)
{
	bool password=true;
	bool user = true;
	Validator v;
	if (v.isPassValid(pass)!=true)
	{
		cout << "1";
		password = false;
		return 1041;
	}
	else if (v.isUserNameValid(userName) != true)
	{
		cout << "2";
		user = false;
		return 1043;
	}
	for (auto const& x : this->UserMap)
	{
		if (x.second->getUserName()==userName)
		{
			//If there is already a user named like this
			cout << "3";
			user = false;
			return 1042;
		}
	}
	if ((user == true) && (password == true))
	{
		return 1040;
	}
}



User * TriviaServer::getUserByName(std::string userName)
{
	for (std::map<SOCKET, User*>::iterator it = UserMap.begin(); it != UserMap.end(); ++it) {
		if (it->second->getUserName() == userName)
		{
			return it->second;
		}
		else {
			return nullptr;
		}

	}
}
User * TriviaServer::getUserBySocket(SOCKET client_socket)
{
	for (std::map<SOCKET, User*>::iterator it = UserMap.begin(); it != UserMap.end(); ++it) {
		if (it->first == client_socket)
		{
			return it->second;
		}
	}
}