
#include "Room.h"

std::string Room::getName()
{
	return this->_name;
}
int Room::getId()
{
	return this->_id;
}
int Room::getQuestionsNo()
{
	return this->_qNo;
}
std::vector<User*> Room::getUsers()
{
	return this->_users;
}
void Room::addUsersToVector(User * user)
{
	this->_users.push_back(user);
}
std::string Room::getUsersListMessage()
{
	int messageCode = 108;
	std::string message = std::to_string(messageCode) + std::to_string(this->playersNumberCurrent);
	for (auto const& value : this->_users) {
		std::string userNameSize = Helper::getPaddedNumber(value->getUserName().length(), 2);
		std::string userName = value->getUserName();
		message += userNameSize + userName;
	}
	return message;
}
std::string Room::joinRoomMessage(User* u)
{
	int messageCode = 110;
	int state;
	
	if (this->playersNumberCurrent == this->_maxUsers)
	{
		state = 1;
	}
	else {
		state = 0;
	}
	
	std::string message = std::to_string(messageCode) + std::to_string(state)+Helper::getPaddedNumber(this->_qNo,2)+Helper::getPaddedNumber(this->_qtime,2);
	std::cout << message;
	return message;

}
Room::Room(int id, User * admin, std::string name, int maxUsers, int questionsNo, int questionTime)
{
	this->_users.push_back(admin);
	this->_id = id;
	this->_admin = admin;
	this->_name = name;
	this->_maxUsers = maxUsers;
	this->_qNo = questionsNo;
	this->_qtime = questionTime;
	this->playersNumberCurrent++;
}

void Room::sendMessage(User * execlude, std::string meesage)
{
	
}

void Room::sendMessage(std::string message)
{
	this->sendMessage(nullptr, message);
}