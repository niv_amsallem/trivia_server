
#pragma once
#include "User.h"
#include <vector>
#include <iostream>
class Room {
public:
	Room(int id, User* admin, std::string name, int maxUsers, int questionsNo, int questionTime);
	std::vector<User*> getUsers();
	std::string getUsersListMessage();//returns a string whick represents message 108
									  //runs through all connected users and builds message to send
	std::string joinRoomMessage(User*);
	int getQuestionsNo();
	int getId();
	std::string getName();
	void addUsersToVector(User*);
	void sendMessage(User* execlude, std::string meesage);
	void sendMessage(std::string message);
private:
	int _qtime;//max q time to answer
	User* _admin;//pointer to the user which is also the admin
	int _qNo;//max q number
	std::vector<User*> _users;
	std::string _name;// room name
	int _id;
	int _maxUsers;//max users allowed in room
	int playersNumberCurrent = 0;
};
